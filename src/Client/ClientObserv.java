package Client;

public interface ClientObserv {
	public void addObserver(ClientObserver observer);
	public void removeObserver(ClientObserver observer);
	public void notifyObservers();
}
