package Client;
import java.util.Comparator;

public class ClientComparatorByPhoneNumber implements Comparator<Client>{

	public int compare(Client client1, Client client2) {
		return client1.getPhoneNumber().compareTo(client2.getPhoneNumber());
	}
}
