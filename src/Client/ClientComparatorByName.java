package Client;
import java.util.Comparator;

public class ClientComparatorByName implements Comparator<Client>{

	public int compare(Client client1, Client client2) {
		return client1.getLastName().compareTo(client2.getLastName());
	}
}
