package Agenda;

public class Meeting {
	private int dogId;
	public Date date;
	public Hour hour;
	
	public Meeting(int dogId, Date date, Hour hour) {
		this.dogId = dogId;
		this.date = date;
		this.hour = hour;
	}
}
