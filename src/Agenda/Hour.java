package Agenda;

public class Hour {
	public int hour;
	public int minute;
	
	public Hour(int hour, int minute){
		this.hour = hour;
		this.minute = minute;
	}
}
