package Agenda;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

	private List<Meeting> schedule;
	
	public Agenda() {
		this.schedule = new ArrayList<Meeting>();
	}
	
	public List<Meeting> getSchedule(){
		return this.schedule;
	}
	
	public void addToSchedule(Meeting meeting) {
		this.schedule.add(meeting);
	}
	
	public boolean isAvailable(Meeting meeting) {
		return true;
	}
}
