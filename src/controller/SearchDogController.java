package controller;

import java.util.Collection;

import Repository.DogRepository;
import view.SearchDogView;
import view.dto.GetDogDTO;

public class SearchDogController {
	
	private DogRepository dogRepo;
	
	public SearchDogController(DogRepository dogRepo){
		this.dogRepo = dogRepo;
		new SearchDogView(this);
	}
	
	public Collection<GetDogDTO> getDogList(){
		return this.dogRepo.getDogList();
	}

	public Collection<GetDogDTO> searchDog(String name) {
		return this.dogRepo.searchDogByName(name);
		
	}
	
	
}
