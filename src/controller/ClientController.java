package controller;

import Repository.ClientRepository;
import view.AddClientView;
import view.dto.AddClientDTO;

public class ClientController {

	private AddClientView view;
	private ClientRepository repository;

	public ClientController(ClientRepository repository) {
		this.view = new AddClientView(this);
		this.repository = repository;
		
	}
	public void addClient() {
		this.view.setVisible(true);
	}
	
	public void addClient(AddClientDTO dto) {
		repository.addClient(dto);
		this.view.setVisible(false);
		repository.notifyObservers();
	}
}
