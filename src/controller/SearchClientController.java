package controller;

import java.util.Collection;

import Repository.ClientRepository;
import view.SearchClientView;
import view.dto.GetClientDTO;

public class SearchClientController {
	
	private ClientRepository clientRepo;
	
	public SearchClientController(ClientRepository clientRepo){
		this.clientRepo = clientRepo;
		new SearchClientView(this);
	}

	public Collection<GetClientDTO> searchClient(int id) {
		return this.clientRepo.getClientInfo(id);
	}
	
	public Collection<GetClientDTO> searchClientByName(String name){
		return this.clientRepo.searchClientByName(name);
	}
	
	public Collection<GetClientDTO> searchClientByPhoneNumber(String phoneNumber){
		return this.clientRepo.searchClientByPhoneNumber(phoneNumber);
	}
}
