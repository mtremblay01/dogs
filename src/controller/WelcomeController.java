package controller;

import java.util.Collection;

import Client.ClientComparatorByName;
import Client.ClientComparatorByPhoneNumber;
import Client.ClientObserver;
import Repository.ClientRepository;
import Repository.DogRepository;
import view.WelcomeView;
import view.dto.GetClientDTO;

public class WelcomeController implements ClientObserver{

	private ClientController clientController;
	private ClientRepository clientRepo;
	private DogRepository dogRepo;
	private WelcomeView welcomeView;
	
	public WelcomeController(ClientRepository clientRepo, DogRepository dogRepo){
		this.clientRepo = clientRepo;
		this.dogRepo = dogRepo;
	}
	
	public void startApplication(){
		this.welcomeView = new WelcomeView(this);
		this.welcomeView.setVisible(true);
	}
	
	public Collection<GetClientDTO> getClientListByName(){
		return this.clientRepo.getClientList(new ClientComparatorByName());		
	}

	public Collection<GetClientDTO> getClientListByPhoneNumber() {
		return this.clientRepo.getClientList(new ClientComparatorByPhoneNumber());
	}
	
	public void addClient() {
		this.clientController.addClient();
	}

	@Override
	public void updateClientList() {
		this.welcomeView.updateClientList();
		
	}

	public void searchClient() {
		new SearchClientController(this.clientRepo);
	}

	public void addDog() {
		new DogController(this.dogRepo, this.clientRepo);
	}

	public void searchDog() {
		new SearchDogController(this.dogRepo);
		
	}
}
