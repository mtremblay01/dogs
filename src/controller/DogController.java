package controller;

import java.util.Collection;

import Client.ClientComparatorByName;
import Repository.ClientRepository;
import Repository.DogRepository;
import view.DogView;
import view.dto.AddDogDTO;
import view.dto.GetClientDTO;

public class DogController {
	
	private DogRepository dogRepo;
	private ClientRepository clientRepo;
	
	public DogController(DogRepository dogRepo, ClientRepository clientRepo){
		this.dogRepo = dogRepo;
		this.clientRepo = clientRepo;
		new DogView(this);
	}
	
	public void addDog(AddDogDTO addDogDTO){
		this.dogRepo.addDog(addDogDTO);
	}

	public Collection<GetClientDTO> getClientListByName(){
		return this.clientRepo.getClientList(new ClientComparatorByName());
	}
}
