package veterinary;

import Repository.ClientDataSeeder;
import Repository.ClientRepository;
import Repository.DogRepository;
import controller.WelcomeController;

public class VeterinaryMainApp {

	public static void main(String[] args) {
		ClientRepository clientRepo = new ClientRepository();
		DogRepository dogRepo = new DogRepository();
		
		new ClientDataSeeder(clientRepo);
		
		WelcomeController appController = new WelcomeController(clientRepo, dogRepo);
		clientRepo.addObserver(appController);
		appController.startApplication();
	}
}
