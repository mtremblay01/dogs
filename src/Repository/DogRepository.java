package Repository;

import java.util.ArrayList;
import java.util.Collection;

import Dog.Dog;
import view.dto.AddDogDTO;
import view.dto.GetDogDTO;

public class DogRepository {
	private Collection<Dog> dogs = new ArrayList<Dog>();
	
	public void addDog(AddDogDTO dogDTO) {
		Dog dog = new Dog(dogDTO.name, dogDTO.breed, dogDTO.owner);
		this.dogs.add(dog);
	}
	
	public int getNbDogs(){
		return this.dogs.size();
	}

	public Collection<GetDogDTO> getDogList() {
		Collection<GetDogDTO> dogsDTO = new ArrayList<GetDogDTO>();
		
		for(Dog dog : this.dogs){
			dogsDTO.add(new GetDogDTO(dog.getID(), dog.getName(), dog.getBreed().name(), dog.getOwner()));
		}
		
		
		return dogsDTO;
	}

	public Collection<GetDogDTO> searchDogByName(String name) {
		Collection<GetDogDTO> dogsDTO = new ArrayList<GetDogDTO>();
		
		for(Dog dog : this.dogs){
			if(dog.getName().equals(name)){
				dogsDTO.add(new GetDogDTO(dog.getID(), dog.getName(), dog.getBreed().name(), dog.getOwner()));
			}
		}
		
		return dogsDTO;
		
	}
}
