package Repository;

import view.dto.AddClientDTO;

public class ClientDataSeeder {
	public static final String CLIENT1_NAME = "F.";
	public static final String CLIENT1_FIRST_NAME = "Alfonse";
	public static final String CLIENT1_PHONE = "345-345-3456";
	
	public static final String CLIENT2_NAME = "E.";
	public static final String CLIENT2_FIRST_NAME = "Conrad";
	public static final String CLIENT2_PHONE = "123-123-1234";
	
	public static final String CLIENT3_NAME = "D.";
	public static final String CLIENT3_FIRST_NAME = "Bob";
	public static final String CLIENT3_PHONE = "234-234-2345";
	
	public static final String CLIENT4_NAME = "C.";
	public static final String CLIENT4_FIRST_NAME = "Dan";
	public static final String CLIENT4_PHONE = "545-345-3456";
	
	public static final String CLIENT5_NAME = "B.";
	public static final String CLIENT5_FIRST_NAME = "Eric";
	public static final String CLIENT5_PHONE = "423-123-1234";
	
	public static final String CLIENT6_NAME = "A.";
	public static final String CLIENT6_FIRST_NAME = "Foo";
	public static final String CLIENT6_PHONE = "634-234-2345";
	
	public ClientDataSeeder(ClientRepository repo){
		repo.addClient(new AddClientDTO(CLIENT1_FIRST_NAME,CLIENT1_NAME, CLIENT1_PHONE));
		repo.addClient(new AddClientDTO(CLIENT2_FIRST_NAME,CLIENT2_NAME, CLIENT2_PHONE));
		repo.addClient(new AddClientDTO(CLIENT3_FIRST_NAME,CLIENT3_NAME, CLIENT3_PHONE));
		
		repo.addClient(new AddClientDTO(CLIENT4_FIRST_NAME,CLIENT4_NAME, CLIENT4_PHONE));
		repo.addClient(new AddClientDTO(CLIENT5_FIRST_NAME,CLIENT5_NAME, CLIENT5_PHONE));
		repo.addClient(new AddClientDTO(CLIENT6_FIRST_NAME,CLIENT6_NAME, CLIENT6_PHONE));
	}
}
