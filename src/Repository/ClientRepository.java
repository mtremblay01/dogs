package Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Client.Client;
import Client.ClientObserv;
import Client.ClientObserver;
import view.dto.AddClientDTO;
import view.dto.GetClientDTO;

public class ClientRepository implements ClientObserv {

	private final List<Client> clientList;
	private final List<ClientObserver> observerList;
	
	public ClientRepository(){
		this.clientList = new ArrayList<Client>();
		this.observerList = new ArrayList<ClientObserver>();
	}
	
	public void addClient(AddClientDTO dtoClient) {
		Client client = new Client(dtoClient.firstName, dtoClient.lastName, dtoClient.phoneNumber);
		this.clientList.add(client);
	}

	public Collection<GetClientDTO> getClientList(Comparator<Client> comparator) {
		Collections.sort(this.clientList, comparator);
		Collection<GetClientDTO> clientList = new ArrayList<GetClientDTO>();
		
		for(Client client : this.clientList){
			clientList.add(new GetClientDTO(client.getID(), client.getFirstName(), client.getLastName(), client.getPhoneNumber()));
		}
		
		return clientList;
	}

	public void addObserver(ClientObserver observer) {
		this.observerList.add(observer);		
	}

	public void removeObserver(ClientObserver observer) {
		this.observerList.remove(observer);
	}

	public void notifyObservers() {
		for (ClientObserver observer : this.observerList) {
			observer.updateClientList();
		}
	}

	public Collection<GetClientDTO> getClientInfo(int id) {
		Collection<GetClientDTO> client = new ArrayList<GetClientDTO>();
		
		for(Client aClient : this.clientList){
			if(aClient.getID() == id){
				client.add(new GetClientDTO(aClient.getID(), aClient.getFirstName(), aClient.getLastName(), aClient.getPhoneNumber()));
			}
		}
		
		if(client.size() == 0) throw new ClientDoesNotExistException();
		
		return client;
	}
	
	public Collection<GetClientDTO> searchClientByName(String name){
		Collection<GetClientDTO> clients = new ArrayList<GetClientDTO>();
		
		for(Client aClient : this.clientList){
			if(aClient.getLastName().equals(name)){
				clients.add(new GetClientDTO(aClient.getID(), aClient.getFirstName(), aClient.getLastName(), aClient.getPhoneNumber()));
			}
		}
		
		if(clients.size() == 0) throw new ClientDoesNotExistException();
		
		return clients;
	}
	
	public Collection<GetClientDTO> searchClientByPhoneNumber(String phoneNumber){
		Collection<GetClientDTO> clients = new ArrayList<GetClientDTO>();
		
		for(Client aClient : this.clientList){
			if(aClient.getPhoneNumber().equals(phoneNumber)){
				clients.add(new GetClientDTO(aClient.getID(), aClient.getFirstName(), aClient.getLastName(), aClient.getPhoneNumber()));
			}
		}
		
		if(clients.size() == 0) throw new ClientDoesNotExistException();
		
		return clients;
	}
}
