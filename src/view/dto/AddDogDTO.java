package view.dto;

import Dog.Breed;

public class AddDogDTO {

	public Breed breed;
	public String owner;
	public String name;
	
	public AddDogDTO(String name, Breed breed, String string) {
		this.name = name;
		this.breed = breed;
		this.owner = string;
	}
}
