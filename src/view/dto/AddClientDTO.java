package view.dto;

public class AddClientDTO {
	
	public String firstName;
	public String lastName;
	public String phoneNumber;
	
	public AddClientDTO(String firstName, String lastName, String phoneNumber){
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
	}
}
