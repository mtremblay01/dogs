package view.dto;

public class GetDogDTO {
	
	public String breed;
	public String owner;
	public String name;
	public int id;
	
	public GetDogDTO(int id, String name, String breed, String string) {
		this.name = name;
		this.breed = breed;
		this.owner = string;
		this.id = id;
	}
}
