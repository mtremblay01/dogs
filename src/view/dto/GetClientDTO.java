package view.dto;

public class GetClientDTO {
	public int id;
	public String firstName;
	public String lastName;
	public String phone;
	
	public GetClientDTO(int id, String firstName, String lastName, String phone){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}
}
