package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Dog.Breed;
import controller.DogController;
import view.dto.AddDogDTO;
import view.dto.GetClientDTO;

public class DogView extends JFrame implements ActionListener{

	static final long serialVersionUID = 1L;

	//Texts
	private static final String VIEW_TITLE = "Ajouter un chien";
	private static final String ADD_DOG_MESSAGE = "Veuillez remplir tous les champs suivants.";
	private static final String BUTTON_ADD_DOG = "Ajouter";
	private static final String LABEL_DOG_NAME = "Nom";
	private static final String LABEL_DOG_BREED = "Race";
	private static final String LABEL_DOG_OWNER = "Propriétaire";
	
	//Window's properties
	private static final Dimension SCREEN_DIMENSION = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int VIEW_WIDTH = 500;
	private static final int VIEW_HEIGHT = 140;
	private static final Point DEFAULT_LOCATION = new Point(SCREEN_DIMENSION.width / 2 - VIEW_WIDTH / 2,
																SCREEN_DIMENSION.height / 2 - VIEW_HEIGHT / 2);
	private static final Dimension DEFAULT_SIZE = new Dimension(VIEW_WIDTH, VIEW_HEIGHT);
	private static final boolean IS_RESIZABLE = true;
	
	//GUI Elements
	private JTextField dogNameField;
	private JComboBox<Breed> dogBreedField;
	private JComboBox<String> dogOwnerComboBox;
	
	//Action
	private static final String ACTION_ADD_DOG = "ACTION_ADD_DOG";
	
	//Attributes
	private Collection<GetClientDTO> clientDTOList;
	private static Breed[] DOG_BREEDS = Breed.values();
	private String[] clientArray;
	private DogController controller;
	
	public DogView(DogController controller){
		super();
		
		this.controller = controller;
		
		this.clientDTOList = this.controller.getClientListByName();
		clientArray = this.clientListToString();
		
		this.initialize();
		this.setupAddDogMessage();
		this.setupAddDogForm();
		this.setupActionPanel();
	}

	private String[] clientListToString() {
		
		List<String> clients = new ArrayList<String>();
		
		for(GetClientDTO client : this.clientDTOList){
			clients.add(client.id + " - " + client.firstName + " " + client.lastName + " (" + client.phone + ")");
		}
		return clients.toArray(new String[0]);
	}

	private void initialize() {
		this.setTitle(VIEW_TITLE);
		this.setLocation(DEFAULT_LOCATION);
		this.setPreferredSize(DEFAULT_SIZE);		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(IS_RESIZABLE);
		this.getContentPane().setLayout(new BorderLayout());
		this.setVisible(true);
		this.pack();
	}

	private void setupAddDogMessage() {
		JPanel addDogMessage = new JPanel(new BorderLayout());
		this.add(addDogMessage);
		
		JLabel message = new JLabel(ADD_DOG_MESSAGE);
		addDogMessage.add(message, BorderLayout.CENTER);		
	}
	
	private void setupAddDogForm() {
		JPanel addDogForm = new JPanel(new GridLayout(0, 2));
		this.add(addDogForm, BorderLayout.CENTER);
		
		JLabel dogNameLabel = new JLabel(LABEL_DOG_NAME);
		addDogForm.add(dogNameLabel);
		
		this.dogNameField = new JTextField();
		addDogForm.add(this.dogNameField);
		
		JLabel dogBreedLabel = new JLabel(LABEL_DOG_BREED);
		addDogForm.add(dogBreedLabel);
		
		this.dogBreedField = new JComboBox<Breed>(DOG_BREEDS);
		addDogForm.add(this.dogBreedField);
		
		JLabel dogOwnerLabel = new JLabel(LABEL_DOG_OWNER);
		addDogForm.add(dogOwnerLabel);
		
		this.dogOwnerComboBox = new JComboBox<String>(this.clientArray);
		addDogForm.add(this.dogOwnerComboBox);
	}
	
	private void setupActionPanel(){
		JPanel actionPanel = new JPanel(new BorderLayout());
		this.add(actionPanel, BorderLayout.SOUTH);
		
		JButton addDogButton = new JButton(BUTTON_ADD_DOG);
		actionPanel.add(addDogButton);
		addDogButton.addActionListener(this);
		addDogButton.setActionCommand(ACTION_ADD_DOG);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		
		if(action.equals(ACTION_ADD_DOG)){			
			this.controller.addDog(new AddDogDTO(this.dogNameField.getText(), (Breed)this.dogBreedField.getSelectedItem(),
																						(String)this.dogOwnerComboBox.getSelectedItem()));
			
			JOptionPane.showMessageDialog(null, "Votre chien à été ajouté. Merci!");
			this.dispose();
		}
	}
}


