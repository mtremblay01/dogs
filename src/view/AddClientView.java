package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ClientController;
import view.dto.AddClientDTO;

public class AddClientView extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	//Texts
	private static final String VIEW_TITLE = "Ajouter un client";
	public static final String CLIENT_NAME_TEXT_FIELD = "Nom";
	public static final String CLIENT_FIRST_NAME_TEXT_FIELD = "Pr�nom";
	public static final String CLIENT_PHONE_TEXT_FIELD = "T�l�phone";
	public static final String ADD_CLIENT_BUTTON =  "Ajouter un client";

	//Window's properties
	private static final Dimension SCREEN_DIMENSION = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int VIEW_WIDTH = 500;
	private static final int VIEW_HEIGHT = 100;
	private static final Point DEFAULT_LOCATION = new Point(SCREEN_DIMENSION.width / 2 - VIEW_WIDTH / 2,
																SCREEN_DIMENSION.height / 2 - VIEW_HEIGHT / 2);
	private static final Dimension DEFAULT_SIZE = new Dimension(VIEW_WIDTH, VIEW_HEIGHT);
	private static final boolean IS_RESIZABLE = true;
	
	//Action
	private static final String ACTION_ADD_CLIENT = "ACTION_ADD_CLIENT";
	
	private JTextField firstNameEdit;
	private JTextField nameEdit;
	private JTextField phoneEdit;
	private ClientController controller;
	
	public AddClientView(ClientController controller) {
		super();
		
		this.controller = controller;
		this.initialize();
		this.setUpComponents();		
	}

	private void initialize() {
		this.setTitle(VIEW_TITLE);
		this.setLocation(DEFAULT_LOCATION);
		this.setPreferredSize(DEFAULT_SIZE);		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(IS_RESIZABLE);
		this.getContentPane().setLayout(new BorderLayout());
	}

	private void setUpComponents() {
		this.setUpAddClientPanel();
		this.pack();
	}

	private void setUpAddClientPanel() {
		JPanel addClientPanel = new JPanel(new FlowLayout());
		this.add(addClientPanel, BorderLayout.CENTER);
		
		
		//JLabel addLabel = new JLabel(ACTION_ADD_CLIENT);
		//addClientPanel.add(addLabel);

		this.firstNameEdit = new JTextField(CLIENT_FIRST_NAME_TEXT_FIELD);
		addClientPanel.add(this.firstNameEdit);
		this.nameEdit = new JTextField(CLIENT_NAME_TEXT_FIELD);
		addClientPanel.add(this.nameEdit);
		this.phoneEdit = new JTextField(CLIENT_PHONE_TEXT_FIELD);
		addClientPanel.add(this.phoneEdit);
		
		JButton addClient = new JButton(ADD_CLIENT_BUTTON);
		addClientPanel.add(addClient);
		addClient.setActionCommand(ACTION_ADD_CLIENT);
		addClient.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		
		if(action.equals(ACTION_ADD_CLIENT)){
			AddClientDTO dto = new AddClientDTO(this.firstNameEdit.getText(), this.nameEdit.getText(), this.phoneEdit.getText());
			
			controller.addClient(dto);
		}
	}

}
