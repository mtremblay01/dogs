package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.SearchDogController;
import view.dto.GetDogDTO;

public class SearchDogView  extends JFrame implements ActionListener{

	
	private static final long serialVersionUID = 1L;
	//Texts
	private static final String VIEW_TITLE = "Rechercher un chien : ";
	private static final String SEARCH_DOG_BUTTON = "Rechercher";
	private static final String DOG_ID_LABEL = "ID";
	private static final String DOG_NAME_LABEL = "Nom";
	private static final String DOG_BREED_LABEL = "Race";
	private static final String DOG_OWNER_LABEL = "Maitre";
	
	//Window's properties
	private static final Dimension SCREEN_DIMENTION = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int VIEW_WIDTH = 500;
	private static final int VIEW_HEIGHT = 250;
	private static final Point DEFAULT_LOCATION = new Point(SCREEN_DIMENTION.width / 2 - VIEW_WIDTH / 2,
																		SCREEN_DIMENTION.height / 2 - VIEW_HEIGHT / 2);
	private static final Dimension DEFAULT_SIZE = new Dimension(VIEW_WIDTH, VIEW_HEIGHT);
	private static final boolean IS_RESIZABLE = true;
	
	//Actions
	private static final String ACTION_SEARCH_DOG = "ACTION_SEARCH_DOG";
	
	
	//GUI Elements
	private JPanel dogInfoPanel;
	private JLabel dogID;
	private JLabel dogName;
	private JLabel dogBreed;
	private JLabel dogOwner;
	private JTextField searchField;
	private int searchFieldWidth = 10;
	
	//Attributes
	private Collection<GetDogDTO> dogDTO;
	
	private SearchDogController controller;
	
	public SearchDogView(SearchDogController controller){
		super();
		
		this.controller = controller;
		
		this.initialize();
		this.setupSearchPanel();
		setupComponents();
		setupDogInfoPanel();
	}
	
	private void initialize() {
		this.setTitle(VIEW_TITLE);
		this.setLocation(DEFAULT_LOCATION);
		this.setPreferredSize(DEFAULT_SIZE);		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(IS_RESIZABLE);
		this.getContentPane().setLayout(new BorderLayout());
		this.setVisible(true);
		this.pack();
	}
	
	public void setupComponents(){
		this.setupSearchPanel();
		this.setupDogInfoPanel();
	}

	private void setupSearchPanel() {
		JPanel searchPanel = new JPanel(new FlowLayout());
		this.add(searchPanel, BorderLayout.NORTH);
		
		this.searchField = new JTextField();
		searchPanel.add(searchField);
		
		this.searchField.setColumns(searchFieldWidth);
		
		JButton searchButton = new JButton(SEARCH_DOG_BUTTON);
		searchPanel.add(searchButton);
		searchButton.addActionListener(this);
		searchButton.setActionCommand(ACTION_SEARCH_DOG);
	}
	
	private void setupDogInfoPanel(){
		this.dogInfoPanel = new JPanel(new GridLayout(0, 4));
		this.add(dogInfoPanel, BorderLayout.CENTER);
		
		JLabel dogIDLabel = new JLabel(DOG_ID_LABEL);
		dogInfoPanel.add(dogIDLabel);
		
		JLabel dogNameLabel = new JLabel(DOG_NAME_LABEL);
		dogInfoPanel.add(dogNameLabel);
		
		JLabel dogLastNameLabel = new JLabel(DOG_BREED_LABEL);
		dogInfoPanel.add(dogLastNameLabel);
		
		JLabel dogPhoneLabel = new JLabel(DOG_OWNER_LABEL);
		dogInfoPanel.add(dogPhoneLabel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
	
		if(action.equals(ACTION_SEARCH_DOG)){
			if(this.dogDTO == null){				
				this.dogID = new JLabel();
				dogInfoPanel.add(this.dogID);
				
				this.dogName = new JLabel();
				dogInfoPanel.add(this.dogName);
				
				this.dogBreed = new JLabel();
				dogInfoPanel.add(this.dogBreed);
				
				this.dogOwner = new JLabel();
				dogInfoPanel.add(this.dogOwner);
			}
			
			this.dogDTO = this.controller.searchDog(this.searchField.getText());
			
			for(GetDogDTO dog : this.dogDTO){
				
				this.dogInfoPanel.add(new JLabel(dog.id + ""));
				this.dogInfoPanel.add(new JLabel(dog.name));
				this.dogInfoPanel.add(new JLabel(dog.breed));
				this.dogInfoPanel.add(new JLabel(dog.owner));
			}
			
			this.revalidate();
			this.repaint();
		}
	}
}
