package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.SearchClientController;
import view.dto.GetClientDTO;

public class SearchClientView extends JDialog implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	//Texts
	private static final String VIEW_TITLE = "Rechercher un client : ";
	private static final String SEARCH_CLIENT_LABEL = "Rechercher selon : ";
	private static final String SEARCH_CLIENT_BUTTON = "Rechercher";
	private static final String CLIENT_ID_LABEL = "ID";
	private static final String CLIENT_FIRST_NAME_LABEL = "Prenom";
	private static final String CLIENT_LAST_NAME_LABEL = "Nom";
	private static final String CLIENT_PHONE_LABEL = "Telephone";
	private static final String CLIENT_SEARCH_METHOD[] = {"ID", "Nom", "Téléphone"};
	
	//Window's properties
	private static final Dimension SCREEN_DIMENTION = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int VIEW_WIDTH = 500;
	private static final int VIEW_HEIGHT = 250;
	private static final Point DEFAULT_LOCATION = new Point(SearchClientView.SCREEN_DIMENTION.width / 2 - SearchClientView.VIEW_WIDTH / 2,
																		SearchClientView.SCREEN_DIMENTION.height / 2 - SearchClientView.VIEW_HEIGHT / 2);
	private static final Dimension DEFAULT_SIZE = new Dimension(SearchClientView.VIEW_WIDTH, SearchClientView.VIEW_HEIGHT);
	private static final boolean IS_RESIZABLE = true;
	
	//Actions
	private static final String ACTION_SEARCH_CLIENT = "ACTION_SEARCH_CLIENT";
	
	
	//GUI Elements
	private JPanel clientInfoPanel;
	private JLabel clientID;
	private JLabel clientFirstName;
	private JLabel clientLastName;
	private JLabel clientPhone;
	private JComboBox<String> searchMethod;
	private JTextField searchField;
	private int searchFieldWidth = 10;
	
	//Attributes
	private SearchClientController controller;
	private Collection<GetClientDTO> clientDTO;
	
	public SearchClientView(SearchClientController controller){
		super();
		
		this.controller = controller;
		
		this.initialize();
		this.setupComponents();
	}
	
	private void initialize() {
		this.setTitle(SearchClientView.VIEW_TITLE);
		this.setLocation(SearchClientView.DEFAULT_LOCATION);
		this.setPreferredSize(SearchClientView.DEFAULT_SIZE);		
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setResizable(SearchClientView.IS_RESIZABLE);
		this.getContentPane().setLayout(new BorderLayout());
		this.setVisible(true);
		this.pack();
	}
	
	public void setupComponents(){
		this.setupSearchPanel();
		this.setupClientInfoPanel();
	}

	private void setupSearchPanel() {
		JPanel searchPanel = new JPanel(new FlowLayout());
		this.add(searchPanel, BorderLayout.NORTH);
		
		JLabel searchClientLabel = new JLabel(SEARCH_CLIENT_LABEL);
		searchPanel.add(searchClientLabel);
		
		this.searchMethod = new JComboBox<String>(CLIENT_SEARCH_METHOD);
		searchPanel.add(this.searchMethod);
		
		this.searchField = new JTextField();
		searchPanel.add(searchField);
		
		this.searchField.setColumns(searchFieldWidth);
		
		JButton searchButton = new JButton(SEARCH_CLIENT_BUTTON);
		searchPanel.add(searchButton);
		searchButton.addActionListener(this);
		searchButton.setActionCommand(ACTION_SEARCH_CLIENT);
	}
	
	private void setupClientInfoPanel(){
		this.clientInfoPanel = new JPanel(new GridLayout(0, 4));
		this.add(clientInfoPanel, BorderLayout.CENTER);
		
		JLabel clientIDLabel = new JLabel(CLIENT_ID_LABEL);
		clientInfoPanel.add(clientIDLabel);
		
		JLabel clientFirstNameLabel = new JLabel(CLIENT_FIRST_NAME_LABEL);
		clientInfoPanel.add(clientFirstNameLabel);
		
		JLabel clientLastNameLabel = new JLabel(CLIENT_LAST_NAME_LABEL);
		clientInfoPanel.add(clientLastNameLabel);
		
		JLabel clientPhoneLabel = new JLabel(CLIENT_PHONE_LABEL);
		clientInfoPanel.add(clientPhoneLabel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
	
		if(action.equals(ACTION_SEARCH_CLIENT)){
			if(this.clientDTO == null){				
				this.clientID = new JLabel();
				clientInfoPanel.add(this.clientID);
				 
				this.clientFirstName = new JLabel();
				clientInfoPanel.add(this.clientFirstName);
				
				this.clientLastName = new JLabel();
				clientInfoPanel.add(this.clientLastName);
				
				this.clientPhone = new JLabel();
				clientInfoPanel.add(this.clientPhone);
			}
			
			// ID
			if(this.searchMethod.getSelectedItem().equals(CLIENT_SEARCH_METHOD[0])){
				this.clientDTO = this.controller.searchClient(Integer.parseInt(this.searchField.getText()));
			}
			
			// Name
			if(this.searchMethod.getSelectedItem().equals(CLIENT_SEARCH_METHOD[1])){
				this.clientDTO = this.controller.searchClientByName(this.searchField.getText());
			}

			//Phone Number
			if(this.searchMethod.getSelectedItem().equals(CLIENT_SEARCH_METHOD[2])){
				this.clientDTO = this.controller.searchClientByPhoneNumber(this.searchField.getText());
			}
			
			this.remove(this.clientInfoPanel);
			this.setupClientInfoPanel();
			
			for(GetClientDTO client : this.clientDTO){
				
				this.clientInfoPanel.add(new JLabel(client.id + ""));
				this.clientInfoPanel.add(new JLabel(client.firstName));
				this.clientInfoPanel.add(new JLabel(client.lastName));
				this.clientInfoPanel.add(new JLabel(client.phone));
			}
			
			this.revalidate();
			this.repaint();
		}
	}
}
