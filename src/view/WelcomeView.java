package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Client.ClientObserver;
import controller.WelcomeController;
import view.dto.GetClientDTO;

public class WelcomeView extends JFrame implements ActionListener,ClientObserver{

	private static final long serialVersionUID = 1L;

	//Text on screen
	public static final String VIEW_TITLE = "Vétérinaire";
	
	public static final String SORT_CLIENT_LABEL = "Trier les clients par : ";
	public static final String[] SORT_CLIENT_METHODS = {"nom", "telephone"};
	
	public static final String SORT_CLIENT_BUTTON = "Trier";
	public static final String ADD_CLIENT_BUTTON =  "Ajouter un client";
	public static final String ADD_DOG_BUTTON =  "Ajouter un chien";
	public static final String SEARCH_CLIENT_BUTTON =  "Rechercher un client";
	public static final String SEARCH_DOG_BUTTON = "Rechercher un chien";
	
	public static final String ID_LABEL = "ID Client";
	public static final String NAME_LABEL = "Nom";
	public static final String FIRST_NAME_LABEL = "Prénom";
	public static final String PHONE_LABEL = "Téléphone";
	
	//Window's properties
	private static final Dimension SCREEN_DIMENSION = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int VIEW_WIDTH = 800;
	private static final int VIEW_HEIGHT = 600;
	private static final Point DEFAULT_LOCATION = new Point(WelcomeView.SCREEN_DIMENSION.width / 2 - WelcomeView.VIEW_WIDTH / 2,
																WelcomeView.SCREEN_DIMENSION.height / 2 - WelcomeView.VIEW_HEIGHT / 2);
	private static final Dimension DEFAULT_SIZE = new Dimension(WelcomeView.VIEW_WIDTH, WelcomeView.VIEW_HEIGHT);
	private static final boolean IS_RESIZABLE = true;
	
	//Action
	public static final String ACTION_LIST_CLIENT = "ACTION_LIST_CLIENT";
	public static final String ACTION_ADD_CLIENT = "ACTION_ADD_CLIENT";
	public static final String ACTION_ADD_DOG = "ACTION_ADD_DOG";
	public static final String ACTION_SEARCH_CLIENT = "ACTION_SEARCH_CLIENT";
	public static final String ACTION_SEARCH_DOG = "ACTION_SEARCH_DOG";
	
	//GUI Elements
	JComboBox<String> sortMethod;
	
	//Attributes
	private WelcomeController controller;
	private Collection<GetClientDTO> clientList;
	private JPanel listClientPanel;
	
	public WelcomeView(WelcomeController controller){
		super();
		
		this.controller = controller;
		this.clientList = this.controller.getClientListByName();
		
		this.initialize();
		this.setUpComponents();
	}

	private void initialize() {
		this.setTitle(WelcomeView.VIEW_TITLE);
		this.setLocation(WelcomeView.DEFAULT_LOCATION);
		this.setPreferredSize(WelcomeView.DEFAULT_SIZE);		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(WelcomeView.IS_RESIZABLE);
		this.getContentPane().setLayout(new BorderLayout());
	}

	private void setUpComponents() {
		this.setUpListClientControlPanel();
		this.setUpListClientPanel();
		this.setUpNavPanel();
		this.pack();
	}

	//Choix de tri
	private void setUpListClientControlPanel() {
		JPanel listClientControlPanel = new JPanel(new FlowLayout());
		this.add(listClientControlPanel, BorderLayout.NORTH);
		
		
		JLabel sortLabel = new JLabel(WelcomeView.SORT_CLIENT_LABEL);
		listClientControlPanel.add(sortLabel);
		
		this.sortMethod = new JComboBox<String>(WelcomeView.SORT_CLIENT_METHODS);
		listClientControlPanel.add(this.sortMethod);
		
		JButton sortClient = new JButton(WelcomeView.SORT_CLIENT_BUTTON);
		listClientControlPanel.add(sortClient);
		sortClient.setActionCommand(WelcomeView.ACTION_LIST_CLIENT);
		sortClient.addActionListener(this);
	}
	
	//Liste des clients
	private void setUpListClientPanel() {
		this.listClientPanel = new JPanel(new BorderLayout());
		this.add(listClientPanel, BorderLayout.CENTER);
		
		listClientPanel.setLayout(new GridLayout(0, 4));
		listClientPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		listClientPanel.setBackground(Color.WHITE);
				

		JLabel clientID = new JLabel(ID_LABEL);
		listClientPanel.add(clientID);

		JLabel clientLastName = new JLabel(FIRST_NAME_LABEL);
		listClientPanel.add(clientLastName);
		
		JLabel clientfirstName = new JLabel(NAME_LABEL);
		listClientPanel.add(clientfirstName);
		
		JLabel clientPhoneNumber = new JLabel(PHONE_LABEL);
		listClientPanel.add(clientPhoneNumber);
		
		for(GetClientDTO client : this.clientList){
			
			JLabel id = new JLabel(client.id + "");
			listClientPanel.add(id);
			
			JLabel lastName = new JLabel(client.firstName);
			listClientPanel.add(lastName);

			JLabel firstName = new JLabel(client.lastName);
			listClientPanel.add(firstName);

			JLabel phoneNumber = new JLabel(client.phone);
			listClientPanel.add(phoneNumber);
		}
	}
	
	//Nav bas
	private void setUpNavPanel() {
		JPanel navPanel = new JPanel();
		this.add(navPanel, BorderLayout.SOUTH);
		
		navPanel.setLayout(new FlowLayout());
		
		JButton addClient = new JButton(ADD_CLIENT_BUTTON);
		navPanel.add(addClient);
		addClient.setActionCommand(ACTION_ADD_CLIENT);
		addClient.addActionListener(this);
		
		JButton searchClient = new JButton(SEARCH_CLIENT_BUTTON);
		navPanel.add(searchClient);
		searchClient.setActionCommand(ACTION_SEARCH_CLIENT);
		searchClient.addActionListener(this);
		
		JButton addDog = new JButton(ADD_DOG_BUTTON);
		navPanel.add(addDog);
		addDog.setActionCommand(ACTION_ADD_DOG);
		addDog.addActionListener(this);
		
		JButton searchDog = new JButton(SEARCH_DOG_BUTTON);
		navPanel.add(searchDog);
		searchDog.setActionCommand(ACTION_SEARCH_DOG);
		searchDog.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		
		if(action.equals(ACTION_LIST_CLIENT)){
			if(this.sortMethod.getSelectedItem().equals(SORT_CLIENT_METHODS[0])){
				this.clientList = this.controller.getClientListByName();
			}
			else if(this.sortMethod.getSelectedItem().equals(SORT_CLIENT_METHODS[1])){
				this.clientList = this.controller.getClientListByPhoneNumber();
			}
			setUpListClientPanel();
		}
		else if(action.equals(ACTION_ADD_CLIENT)){
			this.controller.addClient();
		}
		else if(action.equals(ACTION_ADD_DOG)){
			this.controller.addDog();
		}
		else if(action.equals(ACTION_SEARCH_CLIENT)){
			this.controller.searchClient();
		}
		else if(action.equals(ACTION_SEARCH_DOG)){
			this.controller.searchDog();
		}
	}
	
	public void updateClientList() {
		if(this.sortMethod.getSelectedItem().equals(SORT_CLIENT_METHODS[0])){
			this.clientList = this.controller.getClientListByName();
		}
		else if(this.sortMethod.getSelectedItem().equals(SORT_CLIENT_METHODS[1])){
			this.clientList = this.controller.getClientListByPhoneNumber();
		}
		setUpListClientPanel();
	}
}
