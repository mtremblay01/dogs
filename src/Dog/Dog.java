package Dog;

public class Dog {

	private static int idIncrement = 1;
	private String name;
	private String owner;
	private int id;
	private Breed breed;
	
	public Dog(String name, Breed breed, String owner) {
		this.name = name;
		this.breed = breed;
		this.owner = owner;
		this.id = idIncrement++;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public Breed getBreed() {
		return this.breed;
	}
	
	public int getID() {
		return this.id;
	}
}
