package Tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import Client.Client;
import Dog.Dog;

public class DogTest {

	private Client owner;
	
	@Before
	void setUp() {
		String FIRST_NAME = "jojo";
		String LAST_NAME = "doyon";
		String PHONE_NUMBER = "911";
		this.owner = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
	}

	@Test
	void GIVEN_Dog_WHEN_DogIsInstancied_NameIsSet() {
		String NAME = "jojo";
		Dog.Breed BREED = Dog.Breed.BOUVIER;
		String PHONE_NUMBER = "911";
		Dog dog = new Dog(NAME, BREED, this.owner);
		assertEquals(NAME, dog.getName());
	}

	@Test
	void GIVEN_Dog_WHEN_DogIsInstancied_BreedIsSet() {
		String NAME = "jojo";
		Dog.Breed BREED = Dog.Breed.BOUVIER;
		String PHONE_NUMBER = "911";
		Dog dog = new Dog(NAME, BREED, this.owner);
		assertEquals(BREED, dog.getBreed());
	}

	@Test
	void GIVEN_Dog_WHEN_DogIsInstancied_OwnerIsSet() {
		String NAME = "jojo";
		Dog.Breed BREED = Dog.Breed.BOUVIER;
		String PHONE_NUMBER = "911";
		Dog dog = new Dog(NAME, BREED, this.owner);
		assertEquals(this.owner, dog.getOwner());
	}

	@Test
	void GIVEN_Dog_WHEN_DogIsInstancied_IDIsSet() {
		String NAME = "jojo";
		Dog.Breed BREED = Dog.Breed.BOUVIER;
		String PHONE_NUMBER = "911";
		Dog dog = new Dog(NAME, BREED, this.owner);
		int EXPECTED = dog.getID() + 1;
		Dog dog2 = new Dog(NAME, BREED, this.owner);
		assertEquals(EXPECTED, dog2.getID());
	}
}
