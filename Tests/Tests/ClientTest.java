package Tests;

import static org.junit.jupiter.api.Assertions.*;
import Client.*;

import org.junit.jupiter.api.Test;

class ClientTest {

	@Test
	void GIVEN_Client_WHEN_ClientIsInstancied_FirstNameIsSet() {
		String FIRST_NAME = "jojo";
		String LAST_NAME = "doyon";
		String PHONE_NUMBER = "911";
		Client client = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
		assertEquals(FIRST_NAME, client.getFirstName());
	}

	@Test
	void GIVEN_Client_WHEN_ClientIsInstancied_LastNameIsSet() {
		String FIRST_NAME = "jojo";
		String LAST_NAME = "doyon";
		String PHONE_NUMBER = "911";
		Client client = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
		assertEquals(LAST_NAME, client.getLastName());
	}

	@Test
	void GIVEN_Client_WHEN_ClientIsInstancied_PhoneNumberIsSet() {
		String FIRST_NAME = "jojo";
		String LAST_NAME = "doyon";
		String PHONE_NUMBER = "911";
		Client client = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
		assertEquals(PHONE_NUMBER, client.getPhoneNumber());
	}

	@Test
	void GIVEN_Client_WHEN_ClientIsInstancied_ClientIDIsSet() {
		String FIRST_NAME = "jojo";
		String LAST_NAME = "doyon";
		String PHONE_NUMBER = "911";
		int ID = 1;
		Client client = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
		Client client2 = new Client(FIRST_NAME, LAST_NAME, PHONE_NUMBER);
		assertEquals(ID, client2.getID() - client.getID());
	}

}
